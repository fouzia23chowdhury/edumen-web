/**
*
* Button
*
*/

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class Button extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
      <input type="submit" onSubmit={this.props.onSubmitForm} value="Login"/>
      </div>
    );
  }
}

Button.propTypes = {
  onSubmitForm: PropTypes.func,
};

export default Button;
