/**
*
* InstituteView
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class InstituteView extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <FormattedMessage {...messages.header} />
        <p>Institute Id: {this.props.info.instituteId}</p>
        <p>Institute Name: {this.props.info.instituteName}</p>
        <p>Institute Address : {this.props.info.instituteAddress}</p>
        <p>Institute Package : {this.props.info.institutePackage}</p>
        <p>Institute Create Date : {this.props.info.instituteCreateDate}</p>

      </div>
    );
  }
}

InstituteView.propTypes = {

};

export default InstituteView;
