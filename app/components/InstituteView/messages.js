/*
 * InstituteView Messages
 *
 * This contains all the text for the InstituteView component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.InstituteView.header',
    defaultMessage: 'This is the InstituteView component !',
  },
});
