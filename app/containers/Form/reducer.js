/*
 *
 * Form reducer
 *
 */

import { fromJS } from 'immutable';
import { CHANGE_USERNAME, CHANGE_PASSWORD, CHANGE_ADDRESS, CHANGE_MOBILME, CHANGE_INSTITUTEID,  SET_INSTITUTE_INFO } from './constants';

const initialState = fromJS({
  'username': '',
  'password': '',
  'address': '',
  'mobile': '',
});
//here action any kind for name it just a parameter
function formReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_USERNAME:
      return state.set('username', action.username);
    case CHANGE_PASSWORD:
      return state.set('password', action.password);
    case CHANGE_ADDRESS:
      return state.set('address', action.address);
    case CHANGE_MOBILME:
      return state.set('mobile', action.mobile);
    case CHANGE_INSTITUTEID:
      return state.set('instituteid', action.instituteid);
    case SET_INSTITUTE_INFO:
      return state.set('instituteInfo', action.item);

    default:
      return state;
  }
}

export default formReducer;
