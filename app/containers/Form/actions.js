/*
 *
 * Form actions
 *
 */

import {
  CHANGE_USERNAME,
  CHANGE_PASSWORD,
  CHANGE_ADDRESS,
  CHANGE_MOBILME,
  CHANGE_INSTITUTEID,
  SUBMIT_LOGIN,
  FIND_INSTITUTE_INFO,
  FETCH_INFO,
  SET_INSTITUTE_INFO,

} from './constants';


// Here name parameter must be same as input field value
//when I use changeUserAddress function then we are able to pass a value in parameter
export function changeUserName(username) {
  return {
    type: CHANGE_USERNAME,
    username,
  };
}

// Here password parameter must be same as input field value
//when I use changeUserAddress function then we are able to pass a value in parameter
export function changePassword(password) {
  return {
    type: CHANGE_PASSWORD,
    password,
  };
}

// Here addres parameter must be same as input field value
//when I use changeUserAddress function then we are able to pass a value in parameter
export function changeUserAddress(address) {
  return {
    type: CHANGE_ADDRESS,
    address,
  };
}

// Here mobile parameter must be same as input field value
//when I use changeUserAddress function then we are able to pass a value in parameter
export function changeMobile(mobile) {
  return {
    type: CHANGE_MOBILME,
    mobile,
  };
}

export function changeInstituteId(instituteid) {
  return {
    type: CHANGE_INSTITUTEID,
    instituteid,
  };
}


export function submitLogin() {
  return {
    type: SUBMIT_LOGIN,
  };
}

export function getInfo() {
  return {
    type: FETCH_INFO,
  };
}

export function setInstituteInfo(item) {
  return {
    type: SET_INSTITUTE_INFO,
    item,
  };
}
