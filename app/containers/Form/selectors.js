import { createSelector } from 'reselect';

/**
 * Direct selector to the form state domain
 */
const selectFormDomain = (state) => state.get('form');

/**
 * Other specific selectors
 */

/**
 * Default selector used by Form
 */

const makeSelectUserName = () => createSelector(selectFormDomain, (abc) => abc.get('username'));

const makeSelectPassword = () => createSelector(selectFormDomain, (abc) => abc.get('password'));

const nakeSelectAddress = () => createSelector(selectFormDomain, (abc) => abc.get('address'));

const makeSelectMobile = () => createSelector(selectFormDomain, (abc) => abc.get('mobile'));

const makeSelectInstituteId = () => createSelector(selectFormDomain, (abc) => abc.get('instituteid'));

const makeSelectInstituteInfo = () => createSelector(selectFormDomain, (abc) => abc.get('instituteInfo'));

export {
  selectFormDomain,
  makeSelectUserName,
  makeSelectPassword,
  nakeSelectAddress,
  makeSelectMobile,
  makeSelectInstituteId,
  makeSelectInstituteInfo,
};
