// import { take, call, put, select } from 'redux-saga/effects';

// Individual exports for testing

import { takeLatest, select, call, put } from 'redux-saga/effects';
import { makeSelectUserName, makeSelectPassword, nakeSelectAddress, makeSelectMobile, makeSelectInstituteId } from './selectors';
import { SUBMIT_LOGIN, FETCH_INFO } from './constants';
import { setInstituteInfo } from './actions';
import request from 'utils/request';

export function* doLogin() {
  const username = yield select(makeSelectUserName());
  const password = yield select(makeSelectPassword());
  const address = yield select(nakeSelectAddress());
  const mobile = yield select(makeSelectMobile());
}

export function* getInfoFormApi() {

  const instituteId = yield select(makeSelectInstituteId());

  console.log("Institute ID "+instituteId);
  
  const requestURL = 'http://localhost:8080/institute/setting/institute/view?instituteID=' + instituteId + ' ';

  const info = yield call(request, requestURL);

  yield put(setInstituteInfo(info.item));
}

export default function* login() {

  yield takeLatest(FETCH_INFO, getInfoFormApi);
}
