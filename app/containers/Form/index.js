/**
 *
 * Form
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import {connect, Provider} from 'react-redux';
import {Helmet} from 'react-helmet';
import {FormattedMessage} from 'react-intl';
import {createStructuredSelector} from 'reselect';
import {compose} from 'redux';
import InstituteView from 'components/InstituteView';
import Button from 'components/Button';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { makeSelectInstituteInfo, makeSelectUserName, makeSelectPassword, nakeSelectAddress, makeSelectMobile, makeSelectInstituteId} from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import Input from './Input';
import {changeUserName, changePassword, submitLogin, changeUserAddress, getInfo, changeMobile, changeInstituteId} from './actions';

export class Form extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    let content = 'No data available';
    if (this.props.instituteInfo) {
      content = <InstituteView info={this.props.instituteInfo} />
    } 
    return (
      <div>
        <Helmet>
          <title>Form</title>
          <meta name="description" content="Description of Form"/>
        </Helmet>
        <form onSubmit={this.props.onSearch}>
          <Input
            value={this.props.username}
            onChange={this.props.onChangeUserName}
            name="ussername" /><br/>
         
         <Input
            name="address"
            value={this.props.address}
            onChange={this.props.onChangeUserAddress}/><br/>
        
          <Input
            value={this.props.mobile}
            onChange={this.props.onChangeMobile}/><br/>
         
          <Input
            type="password"
            name="password"
            value={this.props.password}
            onChange={this.props.onChangePassword}/><br/>
          
            <label>Institute ID</label>
            <Input value={this.props.instituteid} onChange={this.props.onChangInstituteId}/><br/>

          <Button />
         
          <input type="submit" onSubmit={this.props.onSearch} value="Search"/>

        </form>
        <div>
          {content}
        </div>
      </div>
    );
  }
}

Form.propTypes = {
  username: PropTypes.string,
  password: PropTypes.string,
  address: PropTypes.string,
  mobile: PropTypes.string,
  instituteid: PropTypes.string,
  onChangeUserName: PropTypes.func,
  onChangePassword: PropTypes.func,
  onChangeUserAddress: PropTypes.func,
  onChangeMobile: PropTypes.func,
  onChangInstituteId: PropTypes.func,
  onSubmitForm: PropTypes.func,
  onSearch: PropTypes.func,

  instituteInfo: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  username: makeSelectUserName(),
  password: makeSelectPassword(),
  address: nakeSelectAddress(),
  mobile: makeSelectMobile(),
  instituteid: makeSelectInstituteId(),
  instituteInfo: makeSelectInstituteInfo(),
});

function mapDispatchToProps(dispatch) {
  return {
    onChangeUserName: (evt) => dispatch(changeUserName(evt.target.value)),
    onChangePassword: (evt) => dispatch(changePassword(evt.target.value)),
    onChangeUserAddress: (evt) => dispatch(changeUserAddress(evt.target.value)),
    onChangInstituteId: (evt) => dispatch(changeInstituteId(evt.target.value)),
    onChangeMobile: (evt) => dispatch(changeMobile(evt.target.value)),

    onSubmitForm: (evt) => {
      if (evt !== undefined && evt.preventDefault) 
        evt.preventDefault();
      dispatch(submitLogin());
    },

    onSearch: (evt) => {
      if (evt !== undefined && evt.preventDefault) 
        evt.preventDefault();
      dispatch(getInfo());
    },
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({key: 'form', reducer});
const withSaga = injectSaga({key: 'form', saga});

export default compose(withReducer, withSaga, withConnect)(Form);
