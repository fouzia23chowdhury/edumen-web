/*
 *
 * Form constants
 *
 */

export const CHANGE_USERNAME = 'app/Form/CHANGE_USERNAME';
export const CHANGE_PASSWORD = 'app/Form/CHANGE_PASSWORD';
export const CHANGE_ADDRESS = 'app/Form/CHANGE_ADDRESS';
export const CHANGE_MOBILME = 'app/Form/CHANGE_MOBILME';
export const CHANGE_INSTITUTEID = 'app/Form/CHANGE_INSTITUTEID';
export const SUBMIT_LOGIN = 'app/Form/SUBMIT_LOGIN';
export const FETCH_INFO = 'app/Form/FETCH_INFO';
export const SET_INSTITUTE_INFO = 'app/Form/SET_INSTITUTE_INFO';
