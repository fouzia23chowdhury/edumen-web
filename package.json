{
  "name": "react-boilerplate",
  "version": "3.5.0",
  "description": "A highly scalable, offline-first foundation with the best DX and a focus on performance and best practices",
  "repository": {
    "type": "git",
    "url": "git://github.com/react-boilerplate/react-boilerplate.git"
  },
  "engines": {
    "npm": ">=3",
    "node": ">=5"
  },
  "author": "Max Stoiber",
  "license": "MIT",
  "scripts": {
    "analyze:clean": "rimraf stats.json",
    "preanalyze": "npm run analyze:clean",
    "analyze": "node ./internals/scripts/analyze.js",
    "extract-intl": "babel-node --presets env,stage-0 -- ./internals/scripts/extract-intl.js",
    "npmcheckversion": "node ./internals/scripts/npmcheckversion.js",
    "preinstall": "npm run npmcheckversion",
    "postinstall": "npm run build-modernizr && npm run build:dll",
    "prebuild": "npm run build:clean",
    "build": "cross-env NODE_ENV=production webpack --config internals/webpack/webpack.prod.babel.js --color -p --progress --hide-modules --display-optimization-bailout",
    "build:clean": "rimraf ./build",
    "build:dll": "node ./internals/scripts/dependencies.js",
    "start": "cross-env NODE_ENV=development node server",
    "start:tunnel": "cross-env NODE_ENV=development ENABLE_TUNNEL=true node server",
    "start:production": "npm run test && npm run build && npm run start:prod",
    "start:prod": "cross-env NODE_ENV=production node server",
    "presetup": "npm i chalk shelljs",
    "setup": "node ./internals/scripts/setup.js",
    "postsetup": "npm run build:dll",
    "clean": "shjs ./internals/scripts/clean.js",
    "clean:all": "npm run analyze:clean && npm run test:clean && npm run build:clean",
    "generate": "plop --plopfile internals/generators/index.js",
    "lint": "npm run lint:js",
    "lint:eslint": "eslint --ignore-path .gitignore --ignore-pattern internals/scripts",
    "lint:js": "npm run lint:eslint -- . ",
    "lint:staged": "lint-staged",
    "pretest": "npm run test:clean && npm run lint",
    "test:clean": "rimraf ./coverage",
    "test": "cross-env NODE_ENV=test jest --coverage",
    "test:watch": "cross-env NODE_ENV=test jest --watchAll",
    "coveralls": "cat ./coverage/lcov.info | coveralls",
    "build-modernizr": "modernizr -c modernizr-config.json -d app/modernizr.js"
  },
  "lint-staged": {
    "*.js": "lint:eslint"
  },
  "pre-commit": "lint:staged",
  "babel": {
    "plugins": [
      "styled-components"
    ],
    "presets": [
      [
        "env",
        {
          "modules": false
        }
      ],
      "react",
      "stage-0"
    ],
    "env": {
      "production": {
        "only": [
          "app"
        ],
        "plugins": [
          "transform-react-remove-prop-types",
          "transform-react-constant-elements",
          "transform-react-inline-elements"
        ]
      },
      "test": {
        "plugins": [
          "transform-es2015-modules-commonjs",
          "dynamic-import-node"
        ]
      }
    }
  },
  "eslintConfig": {
    "parser": "babel-eslint",
    "extends": "airbnb",
    "env": {
      "browser": true,
      "node": true,
      "jest": true,
      "es6": true
    },
    "plugins": [
      "redux-saga",
      "react",
      "jsx-a11y"
    ],
    "parserOptions": {
      "ecmaVersion": 6,
      "sourceType": "module",
      "ecmaFeatures": {
        "jsx": true
      }
    },
    "rules": {
      "arrow-parens": [
        "error",
        "always"
      ],
      "arrow-body-style": [
        2,
        "as-needed"
      ],
      "class-methods-use-this": 0,
      "comma-dangle": [
        2,
        "always-multiline"
      ],
      "import/imports-first": 0,
      "import/newline-after-import": 0,
      "import/no-dynamic-require": 0,
      "import/no-extraneous-dependencies": 0,
      "import/no-named-as-default": 0,
      "import/no-unresolved": 2,
      "import/no-webpack-loader-syntax": 0,
      "import/prefer-default-export": 0,
      "indent": [
        2,
        2,
        {
          "SwitchCase": 1
        }
      ],
      "jsx-a11y/aria-props": 2,
      "jsx-a11y/heading-has-content": 0,
      "jsx-a11y/href-no-hash": 2,
      "jsx-a11y/label-has-for": 2,
      "jsx-a11y/mouse-events-have-key-events": 2,
      "jsx-a11y/role-has-required-aria-props": 2,
      "jsx-a11y/role-supports-aria-props": 2,
      "max-len": 0,
      "newline-per-chained-call": 0,
      "no-confusing-arrow": 0,
      "no-console": 1,
      "no-use-before-define": 0,
      "prefer-template": 2,
      "react/forbid-prop-types": 0,
      "react/jsx-first-prop-new-line": [
        2,
        "multiline"
      ],
      "react/jsx-filename-extension": 0,
      "react/jsx-no-target-blank": 0,
      "react/require-default-props": 0,
      "react/require-extension": 0,
      "react/self-closing-comp": 0,
      "redux-saga/no-yield-in-race": 2,
      "redux-saga/yield-effects": 2,
      "require-yield": 0
    },
    "settings": {
      "import/resolver": {
        "webpack": {
          "config": "./internals/webpack/webpack.prod.babel.js"
        }
      }
    }
  },
  "dllPlugin": {
    "path": "node_modules/react-boilerplate-dlls",
    "exclude": [
      "chalk",
      "compression",
      "cross-env",
      "express",
      "ip",
      "minimist",
      "sanitize.css"
    ],
    "include": [
      "core-js",
      "lodash",
      "eventsource-polyfill"
    ]
  },
  "jest": {
    "collectCoverageFrom": [
      "app/**/*.{js,jsx}",
      "!app/**/*.test.{js,jsx}",
      "!app/*/RbGenerated*/*.{js,jsx}",
      "!app/app.js",
      "!app/global-styles.js",
      "!app/*/*/Loadable.{js,jsx}"
    ],
    "coverageThreshold": {
      "global": {
        "statements": 98,
        "branches": 91,
        "functions": 98,
        "lines": 98
      }
    },
    "moduleDirectories": [
      "node_modules",
      "app"
    ],
    "moduleNameMapper": {
      ".*\\.(css|less|styl|scss|sass)$": "<rootDir>/internals/mocks/cssModule.js",
      ".*\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/internals/mocks/image.js"
    },
    "setupTestFrameworkScriptFile": "<rootDir>/internals/testing/test-bundler.js",
    "testRegex": "tests/.*\\.test\\.js$"
  },
  "dependencies": {
    "@jh3y/whirl": "^1.0.7",
    "animate.css": "3.5.2",
    "babel-polyfill": "6.23.0",
    "blueimp-canvas-to-blob": "3.11.0",
    "blueimp-file-upload": "9.18.0",
    "blueimp-load-image": "2.14.0",
    "blueimp-tmpl": "3.9.0",
    "bootstrap-colorpicker": "2.5.1",
    "bootstrap-filestyle": "1.2.1",
    "bootstrap-slider": "9.8.0",
    "bootstrap-tagsinput": "0.7.1",
    "bootstrap-tour": "0.11.0",
    "bootstrap-wysiwyg": "2.0.1",
    "chalk": "^1.1.3",
    "chart.js": "2.5.0",
    "chartist": "0.11.0",
    "chosen-js": "1.7.0",
    "components-jqueryui": "^1.12.1",
    "compression": "1.6.2",
    "cropper": "3.0.0-rc.1",
    "cross-env": "5.0.0",
    "d3": "4.8.0",
    "datatables": "1.10.13",
    "datatables.net": "1.10.15",
    "datatables.net-bs": "1.10.15",
    "datatables.net-buttons": "1.3.1",
    "datatables.net-buttons-bs": "1.3.1",
    "datatables.net-responsive": "2.1.1",
    "datatables.net-responsive-bs": "2.1.1",
    "easy-pie-chart": "2.1.7",
    "eonasdan-bootstrap-datetimepicker": "4.17.47",
    "ev-emitter": "1.0.3",
    "express": "4.15.3",
    "fastclick": "1.0.6",
    "flatdoc": "0.9.0",
    "font-awesome": "4.7.0",
    "fontfaceobserver": "2.0.9",
    "fullcalendar": "3.4.0",
    "get-size": "2.0.2",
    "history": "4.6.3",
    "hoist-non-react-statics": "2.1.1",
    "html5sortable": "0.5.1",
    "immutable": "3.8.1",
    "inputmask": "3.3.5",
    "intl": "1.2.5",
    "invariant": "2.2.2",
    "ip": "1.1.5",
    "jqGrid": "5.1.1",
    "jqcloud2": "2.0.3",
    "jquery": "2.2.4",
    "jquery-knob": "1.2.11",
    "jquery-localize": "0.1.0",
    "jquery-slimscroll": "1.3.8",
    "jquery-sparkline": "2.4.0",
    "jquery-steps": "1.1.0",
    "jquery-ui": "^1.12.1",
    "jquery-validation": "1.16.0",
    "jquery.easing": "1.4.1",
    "js-storage": "1.0.1",
    "loaders.css": "0.1.2",
    "lodash": "4.17.4",
    "marked": "0.3.6",
    "matchmedia": "0.1.2",
    "minimist": "1.2.0",
    "modernizr": "3.5.0",
    "moment": "2.18.1",
    "morris.js.so": "0.5.1",
    "napa": "2.3.0",
    "outlayer": "2.1.0",
    "parsleyjs": "2.7.1",
    "prop-types": "15.5.10",
    "pubsub-js": "1.5.6",
    "raphael": "2.2.7",
    "react": "15.6.1",
    "react-bootstrap": "0.31.2",
    "react-dom": "15.6.1",
    "react-dropzone": "3.13.1",
    "react-helmet": "5.1.3",
    "react-intl": "2.3.0",
    "react-loadable": "4.0.3",
    "react-redux": "5.0.5",
    "react-router": "4.1.2",
    "react-router-bootstrap": "0.24.2",
    "react-router-dom": "4.1.1",
    "react-router-redux": "5.0.0-alpha.6",
    "react-table": "^6.7.6",
    "react-transition-group": "2.2.0",
    "redux": "3.6.0",
    "redux-immutable": "4.0.0",
    "redux-saga": "0.15.3",
    "reselect": "3.0.1",
    "rickshaw": "1.6.0",
    "sanitize.css": "4.1.0",
    "screenfull": "3.2.0",
    "select2": "4.0.3",
    "select2-bootstrap-theme": "0.1.0-beta.10",
    "simple-line-icons": "2.4.1",
    "spinkit": "1.2.5",
    "styled-components": "2.0.0",
    "sweetalert": "1.1.3",
    "warning": "3.0.0",
    "whatwg-fetch": "2.0.3",
    "x-editable": "1.5.1"
  },
  "devDependencies": {
    "add-asset-html-webpack-plugin": "2.0.1",
    "babel-cli": "6.24.1",
    "babel-core": "6.24.1",
    "babel-eslint": "7.2.3",
    "babel-loader": "7.1.0",
    "babel-plugin-dynamic-import-node": "1.0.2",
    "babel-plugin-react-intl": "2.3.1",
    "babel-plugin-react-transform": "2.0.2",
    "babel-plugin-styled-components": "1.1.4",
    "babel-plugin-transform-es2015-modules-commonjs": "6.24.1",
    "babel-plugin-transform-react-constant-elements": "6.23.0",
    "babel-plugin-transform-react-inline-elements": "6.22.0",
    "babel-plugin-transform-react-remove-prop-types": "0.4.5",
    "babel-preset-env": "1.5.1",
    "babel-preset-react": "6.24.1",
    "babel-preset-stage-0": "6.24.1",
    "circular-dependency-plugin": "3.0.0",
    "coveralls": "2.13.1",
    "css-loader": "0.28.4",
    "enzyme": "2.8.2",
    "eslint": "3.19.0",
    "eslint-config-airbnb": "15.0.1",
    "eslint-config-airbnb-base": "11.2.0",
    "eslint-import-resolver-webpack": "0.8.3",
    "eslint-plugin-import": "2.7.0",
    "eslint-plugin-jsx-a11y": "5.0.3",
    "eslint-plugin-react": "7.0.1",
    "eslint-plugin-redux-saga": "0.3.0",
    "eventsource-polyfill": "0.9.6",
    "exports-loader": "0.6.4",
    "file-loader": "0.11.1",
    "html-loader": "0.4.5",
    "html-webpack-plugin": "2.29.0",
    "image-webpack-loader": "2.0.0",
    "imports-loader": "0.7.1",
    "jest-cli": "20.0.4",
    "lint-staged": "3.5.1",
    "ngrok": "2.2.9",
    "node-plop": "0.7.0",
    "node-sass": "^4.7.2",
    "null-loader": "0.1.1",
    "offline-plugin": "4.8.1",
    "plop": "1.8.0",
    "pre-commit": "1.2.2",
    "raw-loader": "^0.5.1",
    "react-test-renderer": "15.6.1",
    "rimraf": "2.6.1",
    "sass-loader": "^6.0.6",
    "shelljs": "^0.7.8",
    "style-loader": "0.18.1",
    "url-loader": "0.5.8",
    "webpack": "3.5.5",
    "webpack-dev-middleware": "1.11.0",
    "webpack-hot-middleware": "2.18.0"
  }
}
